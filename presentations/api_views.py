from django.http import JsonResponse
from Common.json import ModelEncoder
from .models import Presentation,Status
from events.models import Conference
from django.views.decorators.http import require_http_methods
import json


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]
class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]
    def get_extra_data(self, o):
        return {"status": o.status.name}




@require_http_methods(["GET","POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse({"presentations": presentations}, encoder=PresentationListEncoder)
    try:
        content = json.loads(request.body)
        status = Status.objects.get(name=content["status"])
        content["status"] = status
    except Status.DoesNotExist:
        return JsonResponse(
            {"message":"Invalid"},
            status=400,
        )


@require_http_methods(["GET","DELETE","PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse({"presentations": presentations}, encoder=PresentationListEncoder)
    elif request.method == "DELETE":
        count,_ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(name=content["conference"])
                content['conference'] = conference
        except Conference.DoesNotExist:
            return JsonResponse({"message": "Invalid"},
                                status=400)
